#!/usr/bin/env python3

""" App to manage PipeWire Settings"""
from pw_metadata import PWMetadata
from gui import KissPwTkApp

if __name__ == "__main__":
    pw_metadata = PWMetadata()
    pw_meta_output = pw_metadata.run_pw_dump()
    pw_metadata.get_pw_json_settings(pw_meta_output)
    pw_metadata.extract_pw_settings(
        [
            pw_metadata.srate_node,
            pw_metadata.quantum_node,
            pw_metadata.force_quantum_node
            ]
    )
    print(pw_metadata.settings_dict)
    app = KissPwTkApp(pw_metadata)
    app.mainloop()
