""" Tk GUI to set PipeWire samplerate and quantum """
import pathlib
import os

import tkinter as tk
from tkinter import ttk
import tkinter.font as TkFont
import socket

TK_FONT_SIZE = 18
SRATE_LIST = [16000, 22000, 44100, 48000, 88000, 96000, 192000]
QUANTUM_LIST = [32, 64, 128, 256, 512, 1024, 2048, 4096, 8192]


def set_option_menu_font(root, options_menu, the_font):
    """
    Set the font of an optionmenu both header and options.
    Credit: https://stackoverflow.com/a/63581796
    """
    options_menu.config(font=the_font)
    menu = root.nametowidget(options_menu.menuname)
    menu.config(font=the_font)


class KissPwTkApp(tk.Tk):
    """ The Tk Gui App """
    def __init__(self, pw_meta):
        super().__init__()
        self.pw_meta = pw_meta
        srate = self.pw_meta.settings_dict['clock.rate']
        # If this has never been changed ('forced') then we take the value
        # If it had been changed (e.g. by this or other software) then we take
        # the 'force-quantum' value as the current one
        if self.pw_meta.settings_dict['clock.force-quantum'] != 0:
            quantum = self.pw_meta.settings_dict['clock.force-quantum']
        else:
            quantum = self.pw_meta.settings_dict['clock.quantum']

        self.srate_val = tk.StringVar(self, value=srate)
        self.quantum_val = tk.StringVar(self, value=quantum)

        self.bigfont = TkFont.Font(family='', size=TK_FONT_SIZE)

        self.style = ttk.Style()
        self.style.configure('.', font=self.bigfont)

        self.title('KISS PipeWire Metadata')

        # use absolute path for icon or won't work if called from another dir
        icon_path = os.path.join(
            pathlib.Path(__file__).parent.resolve(),
            'icon.png'
        )
        self.iconphoto(False, tk.PhotoImage(file=icon_path))

        self.make_gui()

    def make_gui(self):
        """ Make the actual GUI """

        # frame for settings
        self.topframe = tk.Frame(self)

        # bottom frame for set button
        self.topframe.pack()
        self.bottomframe = tk.Frame(self)
        self.bottomframe.pack(side=tk.BOTTOM)

        # label for samplerate
        self.label_srate = ttk.Label(self, text='Sample Rate:')
        self.label_srate.pack(side=tk.LEFT, padx=(10, 10))

        # optionmenu for samplerate
        self.combo_srate = tk.OptionMenu(self, self.srate_val, *SRATE_LIST)
        set_option_menu_font(self, self.combo_srate, self.bigfont)
        self.combo_srate.pack(side=tk.LEFT)

        # label for quantum
        self.label_quantum = ttk.Label(self, text='Quantum:')
        self.label_quantum.pack(side=tk.LEFT, padx=(30, 3))

        # optionmenu for quantum
        self.combo_quantum = tk.OptionMenu(
            self,
            self.quantum_val,
            *QUANTUM_LIST
            )
        set_option_menu_font(self, self.combo_quantum, self.bigfont)
        self.combo_quantum.pack(side=tk.LEFT, padx=(0, 10))

        # set button
        self.button = ttk.Button(self.bottomframe, text='Set')
        self.button['command'] = self.button_set_click
        self.button.pack(side=tk.BOTTOM, pady=(20, 10))

        # Apparently this makes it work with something like alltray
        self.client(socket.gethostname())

    def button_set_click(self):
        """
        Set click button calback. Actuall does the pw-metadata setting change
        bu calling the pw_metadata functions
        """
        print('Clicked')
        print(self.srate_val.get())
        print(self.quantum_val.get())
        self.pw_meta.run_set_command(
            self.pw_meta.srate_node,
            self.srate_val.get()
            )
        self.pw_meta.run_set_command(
            self.pw_meta.force_quantum_node,
            self.quantum_val.get()
        )
