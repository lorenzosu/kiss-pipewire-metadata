KISS PipeWire Metadata

A KISS (Keep it Simple, Stupid) Tk GUI tool to quickly set/change the probably
most relevant metadata in PipeWire: sample rate and quantum (aka frames/period).

Run kiss-pw-meta.py

It is just a simple Tk GUI wrapper to run the `pw-metadata` command, get the
values and possibly set them. Having the GUI open allows to do this multiple
times without having to tun a command.

Tested with Python 3.11 and PipeWire 1.0.3

Something like alltray (https://github.com/mbt/alltray) could be used to keep
this gui in the tray (tested but not guaranteed).

Small demo video:

![Sample Video](demo_video/kiss-pw-metadata-demo.mp4)
