""" Functions to get and set PipeWire metadata """
import subprocess
import json
import sys

# The -N commandline option means no colors
PW_DUMP_COMMAND = ['pw-dump', '-N']
PW_META_BASE_COMMAND = ['pw-metadata', '-n', 'settings', '0']


class PWMetadata():
    """
    A basic class to work with PipeWire metadata via commands such as
    pw-dump etc.
    """

    def __init__(self):
        """ Init class """
        self.settings_dict = {}
        self.settings_json = None
        self.srate_node = 'clock.rate'
        self.quantum_node = 'clock.quantum'
        self.force_quantum_node = 'clock.force-quantum'

    def get_pw_json_settings(self, input_string):
        """
        Return the JSON node with PipeWire settings parsing the input_string
        which should be the JSON out put of pw-dump command
        """
        pw_dump_json = json.loads(input_string)
        for node in pw_dump_json:
            try:
                if node['props']['metadata.name'] == 'settings':
                    self.settings_json = node
                    return True
            except KeyError:
                continue
        return False

    def run_pw_dump(self):
        """
        Run pw-dump command defined in PW_DUMP_COMMAND and return the output
        """
        try:
            command_output = subprocess.run(
                PW_DUMP_COMMAND,
                capture_output=True,
                check=False
                )
            return command_output.stdout

        except FileNotFoundError as excp:
            print(f"Trying to run '{PW_DUMP_COMMAND[0]}' command:")
            print(excp)
            print("is PipeWire installed and pw-dump available?")
            sys.exit(1)

    def extract_pw_settings(self, setting_list):
        """
        Extract each setting from json_settings. Try to extract the settings in
        settings_list which is a string list of strings matching the setting
        keys
        """
        found = False
        for this_setting in setting_list:
            for node in self.settings_json['metadata']:
                if this_setting in node['key']:
                    self.settings_dict[node['key']] = node['value']
                    found = True
        return found

    def run_set_command(self, metadata, value):
        """ Set the samplerate to srate via the pw-metadata command """
        command = PW_META_BASE_COMMAND + [metadata, str(value)]

        command_result = subprocess.run(
            command,
            capture_output=True,
            check=False
        )
        print(command_result.stdout.decode())
